# inne-miniprojekty
Zbiór zdjęć oraz wideo mini projektów, które nie nadają się na utworzenie oddzielnych repozytoriów :)
Projekty są przenoszone (nie wszystkie) z github'a założonego na uczelnianego maila: [Github pawelbabiuch](https://github.com/pawelbabiuch) - na wypadek, gdyby tamten mail został usunięty ;)

## Spis mini-projektów
- [Proceduralnie generowany labirynt](#proceduralnie-generowany-labirynt)
- [Prototypy zagubionych projektów](#prototypy-zagubionych-projektów)
  - [Menu Abika](#menu-abika) 
  - [Menu gry mobilnej](#menu-gry-mobilnej) 
  - [Brick Breaker](#brick-breaker)
  - [Robo sorter](#robo-sorter)
  - [RMSEHelper](#rmsehelper)
- [Kursy na youtube](#kursy-na-youtube)
  - [Kurs proceduralnego labiryntu](#kurs-na-youtube)
  - [Kurs baz danych SQL](#kurs-baz-danych-sql)
  

#

## Proceduralnie generowany labirynt
Do generowania labiryntu wykorzystano algorytm z nawrotami (backtracking). Labirynt jest budowany na podstawie losowo generowanego seeda (dzięki czemu - zapisując seed - mamy możliwosć odtworzenia tego samego labiryntu).

### Kurs na youtube

Kurs stworzony dla kanału: [Cziczak Academy](https://www.youtube.com/channel/UClDKjGJE41b32RJKieos_IA)

<a align='center' href="http://www.youtube.com/watch?feature=player_embedded&v=3264A7bUkDs
" target="_blank"><img src="http://img.youtube.com/vi/3264A7bUkDs/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

### Wideoprezentacja

Prezentacja proceduralnie generowanego labiryntu za pomocą rekurencji.

<a align='center' href="http://www.youtube.com/watch?feature=player_embedded&v=GwcVJSs-zGQ
" target="_blank"><img src="http://img.youtube.com/vi/zIFdcT0Iu9U&list/0.jpg" 
alt="wideoprezentujaca #2" border="10" /></a>

#

## Prototypy zagubionych projektów
Masa projektów, która pozostała już tylko na nagraniach: brak plików źródłowych. Pozostałości, do których wraca się z sentymentu.

### Menu Abika
Aplikacja, którą gdzieś zgubiłem przy formacie miała reprezentować interaktywną stronę internetową zbudowaną za pomocą [Silnika graficznego Unity](https://unity3d.com/) na platformie WebGL.

**Wideoprezentacja**

<a align='center' href="http://www.youtube.com/watch?feature=player_embedded&v=ytvNBK6Br_4
" target="_blank"><img src="http://img.youtube.com/vi/ytvNBK6Br_4/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

### Menu gry mobilnej
Prototyp gry został stworzony na potrzeby zlecenia. Grafiki nie są wykonane przeze mnie. Prototyp został wykonany w [Silnik grafizny Unity](https://unity3d.com/) i pozwala przede wszystkim na wybór odpowiedniej mapy, a dla każdej z map wybór odpowiedniego poziomu. Poziomy jak i mapy mogą być zablokowany lub odblokowane. Każdy poziom ma możliwość przejścia na: od 0 do 3 płomyczków (poziomów jakości przejścia)

**Wideoprezentacja**

<a align='center' href="http://www.youtube.com/watch?feature=player_embedded&v=eQEpCDOQBoc
" target="_blank"><img src="http://img.youtube.com/vi/eQEpCDOQBoc/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

### Brick Breaker
Bardzo prowizoryczny prototyp rozgrywki zbudowany na [Silniku grafizny Unity](https://unity3d.com/). Aplikacja wzorowana na [Balls Bricks Breaker](https://play.google.com/store/apps/details?id=balls.arcademaker.bricks.breaker.brounce.ballz). Głównym  celem nie było stworzenie pełnoprawnej aplikacji, a odwzorowanie zachowania. Projekcik robiony głównie dla celów hobbistycznych: jako wyzwanie :)

**Wideoprezentacja**

<a align='center' href="https://youtu.be/-hXEVKZT_ps" target="_blank"><img src="http://img.youtube.com/vi/-hXEVKZT_ps/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

### Robo sorter
Aplikacja zbudowana na [Silniku grafizny Unity](https://unity3d.com/). Aplikacja napisana (z nudów), w celu reprezentacji graficznej algorytmu sortującego, jakim jest Bubble Sort. W pierwszej części filmu wykonywane jest mieszanie (robot losowo podnosi dwa obiekty i je zamienia), a następnie - w drugiej połowie - następuje realizacja algorytmu.

**Wideoprezentacja**

<a align='center' href="https://youtu.be/ZJsvX5jtmGc" target="_blank"><img src="http://img.youtube.com/vi/ZJsvX5jtmGc/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

#

### RMSEHelper
Tym razem [WPF](https://docs.microsoft.com/pl-pl/visualstudio/designers/getting-started-with-wpf?view=vs-2019). Aplikacja została stworzona na potrzeby przedmiotu, w trakcie studiów, o sztucznej inteligencji. Program ten miał za zadanie ułatwić mi budowanie projektów w innym narzędziu - które było zwyczajnie bardzo toporne i problematyczne (byle literówka czy zła nazwa i rzuca błędami), nie wspominając o bardzo wymagającym prowadzącym, który rzucał dwójami za złą kolejność czy małą literę :). To narzędzie zapobiegało w powstawaniu tych nieprzyjemności.
Narzędzie budowane na potrzeby ułatwienia sobie życia, przy projektowaniu w programie [RMSE](http://www.rmse.pl/).

**Wideoprezentacja**

<a align='center' href="https://youtu.be/3WPTIqef2Rs" target="_blank"><img src="http://img.youtube.com/vi/3WPTIqef2Rs/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

#

## Kursy na youtube
Kurs stworzony dla kanału: [Cziczak Academy](https://www.youtube.com/channel/UClDKjGJE41b32RJKieos_IA).

### Kurs baz danych SQL
 Wprowadza niewtajemniczonych w świat relacyjnych baz danych T-SQL. Kurs obejmuje podstawowe zagadnienia i klauzule.
 
 > Ogólne zagadnienia:
 - Czym jest baza danych (przede wszystkim relacyjna)
 - Czym jest dobra baza danych
 - Klauzule: select, from, where, group by, haveing, order by, ...
 - Tworzenie własnej bazy danych
 - Funkcje, procedury, widoki, ...

**[Playlista na youtube](https://www.youtube.com/watch?v=zIFdcT0Iu9U&list=PLk6mhiZKpyW73RVcjcsi8kRMLzKKd55z6)**

#
